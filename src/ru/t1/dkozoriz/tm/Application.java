package ru.t1.dkozoriz.tm;

import ru.t1.dkozoriz.tm.constant.ArgumentConst;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1){
            showError();
            return;
        }
        processArgument(arguments[0]);
    }

    public static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showError();
        }
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Daria Kozoriz");
        System.out.println("E-mail: dkozoriz@t1-consulting.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show version info.\n", ArgumentConst.VERSION);
        System.out.printf("%s - show developer info.\n", ArgumentConst.ABOUT);
        System.out.printf("%s - show command list.\n", ArgumentConst.HELP);
    }

    public static void showError() {
        System.out.println("[ERROR]");
        System.err.println("Current program argument is not correct.");
    }
}
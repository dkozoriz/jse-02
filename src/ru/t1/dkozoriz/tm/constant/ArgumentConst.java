package ru.t1.dkozoriz.tm.constant;

public final class ArgumentConst {

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String HELP = "help";

    private ArgumentConst() {
    }

}